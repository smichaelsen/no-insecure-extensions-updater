<?php

$extensionDataFile = 'extensions_data/extensions.xml';

$extensionData = new SimpleXMLElement(file_get_contents($extensionDataFile));
$insecurePackages = [];

function addInsecurePackageVersion(array $insecurePackages, string $packageName, string $versionString): array
{
    if (!isset($insecurePackages[$packageName])) {
        $insecurePackages[$packageName] = [];
    }
    $insecurePackages[$packageName][] = $versionString;
    return $insecurePackages;
}

function isValidPackageName(string $packageName): bool
{
    return preg_match('/^(\\w|-)+\\/(\\w|-)+$/', trim($packageName));
}

foreach ($extensionData->extension as $extension) {
    /** SimpleXmlElement $extension */
    $extensionKey = (string)$extension->attributes()['extensionkey'];
    foreach ($extension->version as $version) {
        if ((string)$version->reviewstate !== '-1') {
            continue;
        }
        $composerInfoJson = (string)$version->composerinfo;
        if (empty($composerInfoJson)) {
            continue;
        }
        $versionString = (string)$version->attributes()['version'];

        $terPackageName = 'typo3-ter/' . str_replace('_', '-', $extensionKey);
        $insecurePackages = addInsecurePackageVersion($insecurePackages, strtolower($terPackageName), $versionString);

        $composerInfo = json_decode($composerInfoJson, true);
        if (!isset($composerInfo['name']) || empty($composerInfo['name']) || !isValidPackageName($composerInfo['name'])) {
            continue;
        }
        $composerPackageName = $composerInfo['name'];
        $insecurePackages = addInsecurePackageVersion($insecurePackages, strtolower($composerPackageName), $versionString);
    }
}

ksort($insecurePackages);

$insecurePackages = array_map(function (array $versions): string {
    return implode(' || ', $versions);
}, $insecurePackages);

$composerJsonData = [
    'name' => 'smichaelsen/no-insecure-typo3-extensions',
    'description' => 'This package declares conflicts to insecure TYPO3 extensions',
    'authors' => [
        [
            'name' => 'Sebastian Michaelsen',
            'email' => 'sebastian@michaelsen.io',
        ]
    ],
    'conflict' => $insecurePackages,
];
$composerJson = json_encode($composerJsonData, JSON_UNESCAPED_SLASHES ^ JSON_PRETTY_PRINT);
echo $composerJson;
